# Building and Deploying Application Container

## Introduction
This document guides the user to build and deploy the application container.

## Pre-Requisites
- ARM Based Embedded Platform. The following are recommended:
    - Rockchip 3399ProD
    - NXP i.MX 8M Plus
- USB Pen Drive Flashed with ARM System-Ready EWAOL Image with preinstalled docker.
- cn-video and cn-inference containers should be up and running before launching the cn-application container.

## Overview of the document
- Clone the repository
- Deployment of the Application container
- Adding a new device to the WEB UI
- Build the cloud container

## Clone the repository

- Clone the gitlab repo using the below command.
```sh
git clone -b <BRANCH_NAME> <PROJECT_GIT_URL>
```

## Deployment of the Application container
Follow the steps given below for setting up the container.
- Go to the application container folder
```sh
cd <PROJECT_ROOT_FOLDER>/
```
- Open the **config/config.json** file in an editor and update the device name and device link.
  - The device link should be provided in the below format.
    ```sh
    “Dev_link_1” : “http://<IP_ADRESS_OF_DEVICE>:5000”
    ```
    Note that device 1 will be considered as the primary device on web UI. Please update the name accordingly. That is device 1 should be the device on which the application container will be running.
  - Save the file.
- Run the command given below for building the application container.
  ```sh
  docker build -t <IMAGE_NAME>:<TAG_NAME> .
  ```
  Mention the image name & tag name of your choice.
- Check the Docker images list with the command below.
  ```sh
  docker images
  ```
- Deploy the application container using the command below.
  ```sh
  docker run -it --network=host <IMAGE_NAME>:<TAG_NAME> -p <INF_PORT> -c <USB_CAMERA_PATH> -m <MODEL> -ip <INFERENCE_CONTAINER_IP> -csp <CLOUD_ADDRESS> -r <RTSP_STREAM> -csp_s <CLOUD_RTMP_STREAM> -cloud <CLOUD_PLATFORM_OF_CSP>
  ```
    Arguments
    - -p: Port number where the ML inference container is running.
    - -m: Model name that should be loaded for ML inference. Currently **yolov3** and **tiny_yolov3** values are supported.
    - -ip: This is an optional argument which is given if the inference container is deployed on the cloud. The user needs to provide the public IP of the Edge instance as the argument.

      Note that an Edge instance is a virtual machine that contains basic computing components such as the CPU, memory, operating system, network bandwidth, and disks. Once created, you can customize and modify the configuration of an Edge instance.
    - -csp: Cloud container IP and port number. E.g. **127.0.0.1:8080**.
    - -csp_s: This is an optional argument which is given if we want to get live streaming from the cloud. Then, we can provide RTMP pull URL from Apsara Video Live as the argument. For more information, please refer to ApsaraVideo live documentation. If we do not provide csp_s argument, then streaming will happen locally, not from the cloud.
    - -c: Path of the USB camera device.
    - -r: This is also an optional argument which is provided to take input from RTSP (RealTime Streaming Protocol). **Either -c or -r should be provided to take the video input**. And while taking RTSP input, user must provide RTSP path as the argument.
    - -cloud: This argument is used to specify the cloud platform of the csp container if it is running. Supported platforms are aws/alibaba.
    - -nui: This argument is optional and is given when the user wants to run the inference pipeline without web ui integration.


## Adding a new device to the WEB UI
This step is optional. If the user needs to add new devices to the flask web ui then proceed with the following steps.
- On the device where the application container has been built, access the **templates/** directory.
```sh
cd <PROJECT_ROOT>/templates
```
- Open the index.html file in an editor. Add a new line after line number 85 and add the tab for a new device in the following format.
  ```sh
  <li class="nav-item"><a href={{ link_3 }} target="_blank"
        class="nav-link second-tab-toggle">{{ device_3 }}</a></li>
  ```
    Update the device number according to the number of devices being added in the snippet (link_3 and device_3).

- Go to the application container folder.
- Open the **config.json** file in the **config/** folder and create a new entry for device name and device link. The user can refer to the entries created for RK3399 and IMX8MP.
- Open **src/const.py** file.
  - Uncomment line number 20 and 21 for adding a 3rd device.
    ```sh
    # for adding 3rd device uncomment below line.
    # Note that an entry in config/config.json should be created before uncommenting.
    # device_3 = config["Device_3"]
    # dev_link_3 = config["Dev_link_3"]
    ```
  - For adding a 4th device uncomment line number 25 and 26.

- Open **main.py** file in an editor.
  - Uncomment line number 334 and 350 to add a 3rd device to web UI OR uncomment line number 338 and 354 to add a 3rd and a 4th device to Web UI.
    ```sh
    # return render_template('index.html', device_1=device_1, device_2=device_2, device_3=device_3, link_1= dev_link_1 , link_2=dev_link_2, link_3=dev_link_3)

    # return render_template('index1.html', device_1=device_1, device_2=device_2, device_3=device_3, link_1= dev_link_1 , link_2=dev_link_2, link_3=dev_link_3)
    ```
    OR
    ```sh
    # return render_template('index.html', device_1=device_1, device_2=device_2, device_3=device_3, device_4=device_4, link_1= dev_link_1 , link_2=dev_link_2, link_3= dev_link_3, link_4= dev_link_4)

    # return render_template('index1.html', device_1=device_1, device_2=device_2, device_3=device_3, device_4=device_4, link_1= dev_link_1 , link_2=dev_link_2, link_3= dev_link_3, link_4= dev_link_4)
    ```
  - Comment line number 330 and 346.
  ```sh
  return render_template('index.html', device_1=device_1, device_2=device_2, link_1=dev_link_1, link_2=dev_link_2, cloud_service=args["cloud_service"])

  return render_template('index1.html', device_1=device_1, device_2=device_2, link_1=dev_link_1, link_2=dev_link_2)
  ```

The user can refer to the predefined device formats to add new devices.
- Build and run the application container to see the newly added device and tab in the Web UI.

## Build the cloud container
If you want to build and run the application container from the cloud follow the steps provided by the cloud service provider of your choice. We currently support AWS and Alibaba.

Follow either:
- Traffic Monitoring/Cloud Service Provider Integration/Alibaba Cloud/README.md
- Traffic Monitoring/Cloud Service Provider Integration/Amazon Web Services/README.md

You may need to follow several steps before jumping directly into the CSP cloud container deployment.