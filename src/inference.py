import cv2
import grpc
from src.grpc_files import Datas_pb2
from src.grpc_files import Datas_pb2_grpc
import json

def Request(frame):
    ret, buf = cv2.imencode('.jpg', frame)
    if ret != 1:
        return
    yield Datas_pb2.Request(datas=buf.tobytes())

def start_grpc_connection(inf_ip, inf_port):
    channel = grpc.insecure_channel(inf_ip + ":" + inf_port)
    stub = Datas_pb2_grpc.MainServerStub(channel)
    return stub

def send_for_inference(img, stub):
    image_string = cv2.imencode('.jpg', img)[1]
    image_tensor = image_string.tolist()
    resp = stub.getStream(Request(img))
    for i in resp:
        res = i.reply
        data = json.loads(res)
        right_boxes, right_classes, right_scores = data["right_boxes"], data["right_classes"], data["right_scores"]
        model_load_time = data["model_load"]
        pure_inference_time = float("{:.3f}".format(float(data["inf_time"])))
        
    return right_boxes, right_classes, right_scores, model_load_time, pure_inference_time